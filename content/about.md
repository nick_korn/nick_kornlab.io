---
title: About
subtitle: Lyhyt kuvaus minusta
comments: false
date: 2019-01-23
author: Niko
---

{{< image src="https://gitlab.com/nick_korn/nick_korn.gitlab.io/raw/master/static/img/Overflow_1_2_.jpg" alt="Hello Friend" position="center" style="border-radius: 50%;width: 250px;lenght: 250px;" >}}

<br>

***

* __Nimi__: Niko Petteri Liimatainen

* __Syntymäaika__: 13.05.1997

* __Syntymäpaikka__: Kuopio, Suomi

* __Kansallisuus__: Suomi

* __Äidinkieli__: Suomi

* __Muut kielet__: Englanti (Erinomainen), Ruotsi (tyydyttävä)

* __Hyvät puolet__: 
  * Valmis oppimaan uutta ja soveltamaan vanhaa
  * Tulen toimeen helposti uusien ihmisten kanssa ja koen kommunikaation olevan tärkeä osa mitä tahansa ihmissuhdetta
  * Pystyn työskentelemään osana ryhmää sekä yksin
  * Osaan pyytää tarvittaessa apua ja myös tarjota sitä parhaani mukaan

* __Huonot puolet__:

 * Liian perfektionisti joissain asioissa, joita teen
 * Jos jokin ei toimi, pinnani saattaa kiristyä helposti
 * Ajoittainen huijarisyndrooman poteminen
 * Täsmällisyyttä voisi aina parantaa hieman
 
*** 

Alun perin kotoisin Pohjois-Savosta, mutta muutin Jyväskylään saatuani opiskelupaikan Jyväskylän ammattikorkeakoulusta. 

Valmistuin keväällä 2016 ylioppilaaksi Lapinlahden lukio ja kuvataidelukiosta, sen perinteiseltä linjalta. Aloitin opintoni JAMK:ssa samana syksynä muutettuani Jyväskylään. Koska aloitin jatko-opinnot suoraan lukion päätyttyä, en ole vielä suorittanut asepalvelustani.

Harrastuksiini kuuluvat: kirjojen sekä mangan lukeminen, tietokonepelit, intervallijuoksu keväästä syksyyn ja kokkaus.

***

# Ota yhteyttä

* [LinekdIn](https://www.linkedin.com/in/niko-liimatainen-00ab91138/)
* Mail: nickkorn4@gmail.com
