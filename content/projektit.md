---
title: Omat projektit
subtitle: Lyhyt kuvaus minusta
comments: false
date: 2019-01-23
author: Niko
---

Olen myös vapaa-ajalla harrastanut jonkin verran tietoverkkojen ja niiden palveluiden parissa. Kotonani sijaitsee vanhalle tietokoneelleni sijoitettu palvelinympäristö, joka on virtualisoitu Proxmoxin avulla. Ympäristössäni on tällä hetkellä Samba verkkojako tiedostojen jakamista ja varmuuskopiointia varten, oma Gitlab instance sekä Nginxllä toteutettu reverse proxy, jonka avulla proxyän sekä HTTP että SSH yhteyksiä Gitlab palvelimelle. Gitlab instanssini ei ole vielä julkiverkossa saatavilla.

Käytän myös päivittäin eri Linux distribuutioita esim. omalla kannettavalla tietokoneellani sekä pöytäkoneessani dual bootissa. Myös useiden eri palvelin distribuutioiden käyttö on minulle tuttua. 
Tällä hetkellä käytössäni on:

* Palvelimet: CentOS
* Pöytäkone: Ubuntu Budgie/Windows 10 dual boot
* Kannettava tietokone: Manjaro
