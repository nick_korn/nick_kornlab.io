---
title: Harjoittelut
subtitle: Lyhyt kuvaus minusta
comments: false
date: 2019-01-23
author: Niko
---

Tähän mennessä olen suorittanut harjoittelustani pakolliset 30op (n. 750h), kahden kesän aikana.

Suoritin molemmat harjoittelukertani __WIMMA Lab__ -nimisessä projektissa. WIMMA Lab on toteutettu jo useamman kesän ajan Jyvsäkylän ammattikorkeakoulun tiloissa ja on aikaisemmin kulkenut mm. Challenge Factory -nimellä. 

* [Linkki WIMMA Labin kotisivuille, jossa lisätietoa projektista.](http://www.wimmalab.org/ "WIMMA Lab etusivu")

***

## Kesä 2018

Suoritin harjoitteluni toisen osan 16.5.-31.7.2018 välisenä aikana.

2018 vuoden kesänä työskentelin osana __Overflow__ nimistä virtuaaliyritystä, junior developer tittelillä. Overflowin toimeksiantona sinä kesänä oli toteuttaa sairaalan apuvälineiden sijainnin- ja käytönseurantajärjestelmä. Toimeksianto tuli ryhmälle Keski-Suomen sairaanhoitopiirin apuvälinekeskukselta, joka halusi saada tavan, jolla seurata heidän lainaamiaan apuvälineitä sekä myös sitä, käytettiinkö lainattuja apuvälineitä. Toteutimme myös kyseistä toimeksiantoa yhdessä __Telian__ kanssa. He tarjosivat ryhmällemme toteutuksessamme käytettyä työkaluja, kuten: Telian Cumulocity IoT-alustan, GPS-paikantimia ja __Narrow Band__ (NB) IoT-modeemin.

Overflown sisällä olin osana kahden miehen tiimiä, jonka vastuulla oli kehittää liikkeentunnistin ominaisuutta tuotekokonaisuuteemme. Ominaisuuden oli tarkoitus tunnistaa apuvälineeseen kohdistuva liike ja sen avulla ilmoittaa apuvälineen liikkumisesta järjestelmälle. Alla kuvattuna ominaisuuden rakenne.

***

![Here should be a great diagram of a certain feature](https://gitlab.com/nick_korn/nick_korn.gitlab.io/raw/master/static/img/usageRate.png "Liikkeentunnistin ominaisuus")

***

Toisen harjoitteluni aikana opin paljon ohjelmistokehityksestä ja sen dokumentoinnista. Käytin harjoittelussa pääasiallisesti jälleen Pythonia ohjelmoinnissa ja vastasin pääosin clientin yhteydestä serveriin. Ominaisuuden clientin ja serverin toteutin Pythonin socket-kirjastolla, jonka avulla pystyy tekemään TCP tai UDP socketteja datan vastaanottamiseen tietoverkkojen yli. Ominaisuuden kehittämisen ohessa työnkuvaani kuului myös IT-palveluiden pystytys pilveen, ominaisuuden yksikkötestaus ja uusien WIMMA Lab harjoittelijoiden neuvonta kesän alussa. Toimin myös kesän Overflown varavetäjänä. 


* [Linkki OverFlown vuoden 2018 verkkosivulle.](http://www.overflow.wimmalab.org/)

* [Linkki Overflown projektin yleiseen dokumentaatioon.] (https://gitlab.labranet.jamk.fi/OverFlow/Dokumentaatio "OverFlow GitLab repsitorio")

* [Linkki liiketunnistin ominaisuuden repositorioon.](https://gitlab.labranet.jamk.fi/OverFlow/NB-Modem "NB-Modem repositorio")

***

## Kesä 2017

Suoritin ensimmäisen osan harjoittelusta 15.5. - 28.7.2017 välisenä aikana.

Kyseisenä kesänä työskentelin osana __Mysticons__ nimistä virtuaaliyritystä, junior statistician tittelillä. Mysticons oli WIMMA Labin tutkimus- ja kehittämisryhmä, jonka vastuulla oli data-analytiikka, AI ja IoT. Oman ryhmäni työkuvaan kuului __data-analytiikka__ ja sen opiskelu sekä sen toteuttamiseen kehitettyjen tekniikoiden tutkiminen ja soveltaminen toimeksiantoomme. Pääasiallinen toimeksiantomme oli tutkia ja raportoida data-analytiikkaa ja sen työkaluja.

Harjoitteluni aikana käytin pääasiassa Python 3 ohjelmointikieltä, yhdessä Apachen Spark frameworkin kanssa, data-analysoinnin tekemiseen. Pääsin myös tutustumaan NoSQL -tietokantoihin, IoT-datan keruuseen ja analysointi klusterin pystyttämiseen. 

***

Ensimmäisen harjoittelun aikana opin paljon enemmän käytännössä Linux pohjaisista käyttöjärjestelmistä, sekä ohjelmoinnin perusperiaatteista Pythonin avulla.

* [Linkki Mysticonsin vuoden 2017 verkkosivuille.](https://wimmalab.github.io/mysticons/ "Mysticons sivut")

* [Linkki data-analyysi projektin Gitlab repositorioon.](https://cybertrust.labranet.jamk.fi/data-analysis/documentation "Data-analyysi repositorio")

***


