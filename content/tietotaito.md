---
title: Tietotaitoni
subtitle: Lyhyt kuvaus minusta
comments: false
date: 2019-05-07
author: Niko
---


__Tietoverkkotekniikat:__

 * Verkkoprotokollat ja -infrastruktuuri
    * Reititysprotokollat, TCP/UDP, DNS, DHCP yms.
 * Lähi- ja palvelinverkkojen suunnittelu
 * Verkkopalveluiden konfigurointi ja ylläpito
  * DHCP, DNS, tiedostopalvelimet, verkkopalvelimet, proxyt yms.
 * Docker ja pilviratkaisut
  * Ansible, Vagrant, AWS, GCE
 
__Ohjelmistotekniikka:__
 
 * Käytetyimmät ohjelmointikielet: Python ja C#
  * Kiinnostaa oppia ja kehittyä molemmissa. C# vähemmän käytetty
 * Olio-ohjelmointi
 * Protokolla-ohjelmointi
  * TCP/UDP socketit ja niillä tehdyt clientit ja serverit
 * Ohjelmistosuunnittelu:
  * Vaatimusmäärittely ja sen osa-alueet kuten: asiakasprofiilit, palvelupolut ja eri tyyppiset sidosryhmät.

__Kyberturvallisuus:__

 * Teoriaa
  * CIA, eri hyökkäystyypit, heikkoudet yms.
 * Suojausmenetelmät ja algoritmit
 * Digital Forensics ja Incidence Response 
 * Tietoverkkojen turvallisuus
  * Koventaminen, auditointi
 * Tietoturvakomponenttien konfigurointi
  * Palomuurit, honeypotit, IDS ja IPS yms.
 * Kyberharjoitukset
  * Eri harjoitustyypit: Keskustelupohjaiset, tekniset ja toiminallistekniset harjoitukset  
  * Blue/Red/White/Green tiimit ja niiden osuus harjoituksissa
  * Master Scenario Event List (MSEL)
  * Harjoitusympäristön suunnittelu ja toteutus prosessit

__Työskentelymenetelmät ja projektinhallinta:__

 * Scrum
 * Kanban
 * Git
