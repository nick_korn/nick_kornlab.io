---
title: Moduuliopinnot
subtitle: Lyhyt kuvaus minusta
comments: false
date: 2019-05-07
author: Niko
---

Tieto- ja viestintätekniikan opintoihin sisältyy normaalisti kolmannelle vuodelle sijoitetut moduuliopinnot. Moduulit koostuvat useammasta saman aiheen opintojaksosta, jotka on koottu yhdeksi isoksi kokonaisuudeksi. Jyväskylän ammattikorkeakoulu tarjoaa useita vaihtoehtoja moduuleille, joista opiskelijat valitsevat vähintään yhden pakollisen ja halutessaan enemmän.

Valitsin kolmannelle vuodelleni kaksi moduulia: IT-palveluiden tietoturvallinen suunnittelu ja tuotanto sekä kyberharjoituksen.

***

## Kyberharjoitus

Kyberharjoitus koostuu kahdesta opintojaksosta:

* Kyberharjoituksen suunnittelu ja valmistelu ([Linkki JAMK:in opintojaksokuvaukseen](https://asio.jamk.fi/pls/asio/asio_ectskuv1.kurssin_ks?ktun=TTKW0310&knro=&noclose=%20&lan=f))

* Kyberharjoituksen toteutus ([Linkki JAMK:in opintojaksokuvaukseen](https://asio.jamk.fi/pls/asio/asio_ectskuv1.kurssin_ks?ktun=TTKW0320&knro=&noclose=%20&lan=f))

### Kyberharjoituksen suunnittelu ja valmistelu

Opintojakso sijoittui kolmannen lukuvuoteni syksylle. Opintojakson tarkoituksena oli ryhmätyönä suunnitella kyberharjoitus jollekin kuvitteelliselle yritykselle. Ryhmämme valitsi yritystyypiksi sähköntuotanto- ja jakeluyrityksen.

Opintojaksolla myös valittiin yksi ryhmä, joka toteuttaa suunnittelemansa harjoituksen. Ryhmämme suunnitelma valittiin ja toteutimme harjoituksen 19.11.2018

 Oma työnkuvani harjoitusympäristön kasauksessa oli simuloida sähkönjakelu verkoista löytyvää SCADA-käytönohjaus järjestelmää. SCADA server simuloitiin ympäristöön käyttämällä Python 3:a ja siitä löytyvää TCP-socket kirjastoa. SCADA clientit taas simuloitiin Ciscon Packet Traceristä löytyvien IoT-simulaatioiden avulla, joilla lähetettiin dataa TCP-socketin yli serverille. Integroin myös ELK-stackin osaksi SCADA-järjestelmää, jotta dataa saataisiin visualisoitua peliin osallistuville osapuolille. Osallistuin myös harjoittelun yleiseen suunnitteluun ja dokumentointiin, sekä harjoituksen toteutuksessa toimin osana harjoitusta valvovaa joukkuetta.

 * [Opintojaksolla tuotettu harjoitussuunnitelma.](https://gitlab.com/nick_korn/nick_korn.gitlab.io/raw/master/static/Files/R2_Harjoitussuunnitelma.pdf/)

### Kyberharjoituksen toteutus

 Kyberharjoitus moduuli jatkuu kolmannen opintovuoteni keväälle saakka, jolloin vuorossa on kyberharjoituksen toteutus opintojakso. Opintojakson tarkoituksena on osallistua JAMK:in Ylemmässä Ammattikorkeakoulussa opiskelevien opiskelijoiden suunnittelemaan harjoitukseen. Opintojakson ensimmäinen kontakti on 15.2.2019.

 Opintojakson aikana pääsin osaksi harjoituksen Red Team -ryhmittymää. Kevään aikana tutkimme harjoitusverkossa olevia verkkopalveluita etsien niistä haavoittuvuuksia ja pyrimme niiden avulla toteuttamaan mielekkäitä injektioita itse harjoituksen aktiivivaihetta varten.

 Harjoituksen aktiivivaihe oli 19. - 20.4.2019 välisenä aikana. Tämän vaiheen aikana pääasiallinen toimenkuvani oli olla Red Teamin sisällä olleen APT -pienryhmän vetäjä. Pääasaillinen toimenkuvani aktiivivaiheen aikana oli ohjata APT -tekijöille annettuja injektioita ja dokumentoida niiden etenemistä sekä välittää infoa niistä Red Teamin johtajalle ja White Teamille.

***

## IT-palveluiden tietoturvallinen suunnittelu ja tuotanto

 Moduuli koostuu kahdesta opintojaksosta:

 * Tietoturvakontrollien suunnittelu ja toteutus. ([Linkki JAMK:in opintojaksokuvaukseen](https://asio.jamk.fi/pls/asio/asio_ectskuv1.kurssin_ks?ktun=TTKW0110&knro=&noclose=%20&lan=f))
 * Yrityksen infrastruktuuripalvelut. ([Linkki JAMK:in opintojaksokuvaukseen](https://asio.jamk.fi/pls/asio/asio_ectskuv1.kurssin_ks?ktun=TTTW0310&knro=&noclose=%20&lan=f))

 Moduulin molemmat opintojaksot sijoittuivat opintojeni kolmannen vuoden keväälle. 

 Moduulin tarkoituksena on tarkoitus toteuttaa ryhmätyönä jollekin kuvitteelliselle yritykselle sen tietoverkkoinfrastruktuuri ja sinne sijoitettavat palvelut. Moduulin nimen mukaisesti palvelut suunnitellaan ja toteutetaan mahdollisimman tietoturvallisesti.

 Ensimmäisen toimeksiannon aikana ryhmämme muodosti perustan yrityksen verkolle suunnittelemalla ja toteuttamalla sinne AD-infrastruktuurin sekä muita sisäverkkojen peruspalveluita kuten DHCP:n, DNS:n ja File Servereitä. Minun vastuullani oli ensimmäisen toimeksiannon aikana suunnitella ja toteuttaa DNS- ja PKI-palvelimet. Osallistuin osittain myös muiden palveluiden ideointiin ja suunnitteluun.

 Toisen toimeksiannon tavoitteena oli laajentaa verkko lisäämällä uusia palveluita ja teknologioita kuten: DMZ, intra- ja websitet sekä e-mail palvelimet. Vastuullani tässä toimeksiannossa oli toteuttaa ja suunnitella: NTP-palvelin, DMZ-verkko ja DNS-palvelun päivitys uusien sisäisten ja ulkoisten palveluiden nimillä.

 Kolmannessa toimeksiannossa alettiin koventaa yrityksen infrastruktuuria. Tässä toimeksiannossa minun vastuullani oli yrityksen palomuurauksen suunnittelu ja toteuttaminen.

 Viimeisessä toimeksiannossa luotiin infrastruktuurin viimeisiä palveluita ja laajennettiin verkkoa käyttämään JAMK:in tiloissa sijaitsevia SpiderNet -laboratorioverkon fyysisiä laitteita. Viimeisen toimeksiannon aikana suunnittelin yrityksen lokituspolitiikan ja toteutin lokitusinfrastruktuurin sen pohjalta.

 * [Opintojakson lopullisen tuotteen dokumentaatio.](https://gitlab.com/nick_korn/nick_korn.gitlab.io/raw/master/static/Files/Ryhma6_IT-infra.pdf)
