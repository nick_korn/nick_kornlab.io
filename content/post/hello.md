
+++
title = "Portfolio - Niko Liimatainen"
date = "2019-02-13"
#author = "Niko"
cover = ""
description = """

<center>![Kuva minusta?](img/naama2.png)</center>

<br>

Tervetuloa portfoliooni. Tällä sivulla esitellään minua eli Niko Liimataista henkilönä, kerrotaan opinnoistani, työharjoitteluistani sekä omista projekteistani.

<br>

"""
+++

{{< image src="https://gitlab.com/nick_korn/nick_korn.gitlab.io/raw/master/static/img/Overflow_1_2_.jpg" alt="Hello Friend" position="center" style="border-radius: 50%;width: 250px;lenght: 250px;" >}}

<br>

Opiskelen tällä hetkellä kolmatta vuotta __kyberturvallisuuden__ suuntautumista Jyväskylän ammattikorkeakoulussa tieto- ja viestintätekniikan opintoja. Odotettu valmistumisaikani on kevät 2020, mutta tavoittelen valmistuvani jouluna 2019. Minulla ei ole vielä opinnäytetyön aihetta.

Työskentelen tällä hetkellä Security Operations Center Analystinä Viria Security Oy:ssä.
***
